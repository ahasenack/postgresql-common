# Translation of postgresql-common.po to Russian
# This file is distributed under the same license as the postgresql-common
# package.
#
# Yuriy Talakan' <yt@amur.elektra.ru>, 2006.
# Yuriy Talakan' <yt@drsk.ru>, 2007.
# Sergey Alyoshin <alyoshin.s@gmail.com>, 2008, 2016.
#
msgid ""
msgstr ""
"Project-Id-Version: postgresql-common_178_ru\n"
"Report-Msgid-Bugs-To: postgresql-common@packages.debian.org\n"
"POT-Creation-Date: 2016-03-05 11:47+0100\n"
"PO-Revision-Date: 2016-02-22 22:03+0300\n"
"Last-Translator: Sergey Alyoshin <alyoshin.s@gmail.com>\n"
"Language-Team: Russian <debian-l10n-russian@lists.debian.org>\n"
"Language: ru\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: KBabel 1.9.1\n"
"Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n"
"%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);\n"

#. Type: error
#. Description
#: ../postgresql-common.templates:1001
msgid "Obsolete major version ${old}"
msgstr "Устаревшая версия ${old}"

#. Type: error
#. Description
#: ../postgresql-common.templates:1001
msgid ""
"The PostgreSQL version ${old} is obsolete, but the server or client packages "
"are still installed. Please install the latest packages (postgresql-"
"${latest} and postgresql-client-${latest}) and upgrade the existing "
"${oldversion} clusters with pg_upgradecluster (see manpage)."
msgstr ""
"PostgreSQL версии ${old} устарел, но пакет сервера или клиента все ещё "
"установлен. Пожалуйста, установите последние пакеты (postgresql-${latest} и "
"postgresql-client-${latest}), и обновите ваши существующие кластеры "
"${oldversion} с помощью pg_upgradecluster (см. man-страницу)."

#. Type: error
#. Description
#: ../postgresql-common.templates:1001
msgid ""
"Please be aware that the installation of postgresql-${latest} will "
"automatically create a default cluster ${latest}/main. If you want to "
"upgrade the ${old}/main cluster, you need to remove the already existing "
"${latest} cluster (pg_dropcluster --stop ${latest} main, see manpage for "
"details)."
msgstr ""
"Учтите, что установка postgresql-${latest} автоматически создаст кластер по "
"умолчанию ${latest}/main. Если вы желаете обновить кластер ${old}/main, то "
"должны удалить существующий кластер ${latest} с помощью pg_dropcluster --"
"stop ${latest} main (см. man-страницу)."

#. Type: error
#. Description
#: ../postgresql-common.templates:1001
msgid ""
"The old server and client packages are no longer supported. After the "
"existing clusters are upgraded, the postgresql-${old} and postgresql-client-"
"${old} packages should be removed."
msgstr ""
"Старые пакеты сервера и клиента более не поддерживаются. После обновления "
"существующих кластеров, пакеты postgresql-${old} и postgresql-client-${old} "
"должны быть удалены."

#. Type: error
#. Description
#: ../postgresql-common.templates:1001
msgid ""
"Please see /usr/share/doc/postgresql-common/README.Debian.gz for details."
msgstr ""
"Для дальнейшей информации см. /usr/share/doc/postgresql-common/README.Debian."
"gz"

#. Type: boolean
#. Description
#: ../postgresql-common.templates:2001
msgid "Enable SSL by default in new PostgreSQL clusters?"
msgstr "Включить SSL по умолчанию в новых кластерах PostgreSQL?"

#. Type: boolean
#. Description
#: ../postgresql-common.templates:2001
msgid ""
"PostgreSQL supports SSL-encrypted connections. This is usually a good thing. "
"However, if the database is solely accessed using TCP connections on "
"localhost, SSL can be turned off without introducing security issues."
msgstr ""
"PostgreSQL поддерживает соединения с SSL-шифрованием. Обычно это хорошо. Но, "
"если база данных адресуется только по TCP-соединению на локальном "
"компьютере, то SSL может быть выключен без повышения угроз безопасности."

#. Type: boolean
#. Description
#: ../postgresql-common.templates:2001
msgid ""
"UNIX domain socket connections (called \"local\" in pg_hba.conf) are not "
"affected by this setting. This setting concerns new PostgreSQL clusters "
"created during package install, or by using the pg_createcluster command. It "
"does not reconfigure existing clusters."
msgstr ""
"Соединения сокета домена UNIX (называемые \"local\" в pg_hba.conf) не будут "
"затронуты этой настройкой. Она касается новых кластеров PostgreSQL, "
"созданных при установке пакета или командой pg_createcluster. Существующие "
"кластеры перенастроены не будут."

#. Type: boolean
#. Description
#: ../postgresql-common.templates:2001
msgid "If unsure, enable SSL."
msgstr "Если не уверены, включите SSL."

#. Type: note
#. Description
#: ../postgresql-common.templates:3001
msgid "PostgreSQL ${version} catalog version changed"
msgstr "Изменена версия каталога PostgreSQL ${version}"

#. Type: note
#. Description
#: ../postgresql-common.templates:3001
msgid ""
"The PostgreSQL cluster ${version} ${cluster} was created using catalog "
"version ${db_catversion}, but the currently being installed package "
"postgresql-${version} is using catalog version ${new_catversion}. You will "
"not be able to use this cluster until it was upgraded to the new catalog "
"version."
msgstr ""
"Был создан кластер ${cluster} PostgreSQL ${version} с использованием "
"каталога версии ${db_catversion}, но устанавливаемый сейчас пакет postgresql-"
"${version} использует каталог версии ${new_catversion}. Вы не сможете "
"использовать данный кластер, пока не обновите его до каталога новой версии."

#. Type: note
#. Description
#: ../postgresql-common.templates:3001
msgid ""
"The necessary subset of binaries from the old version was saved in "
"${vartmpdir}. To upgrade the cluster, execute these commands:"
msgstr ""
"Необходимый набор двоичных файлов из старой версии был сохранён в "
"${vartmpdir}. Для обновления кластера выполните следующие команды:"
